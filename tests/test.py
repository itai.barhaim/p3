import os
import io
import pytest
import requests
from PIL import Image

from applitools.images import Eyes, BatchInfo, BatchClose, logger

logger.set_logger(logger.StdoutLogger())


@pytest.fixture(name="eyes", scope="function")
def eyes_setup():
    """
    Basic Eyes setup. It'll abort test if wasn't closed properly.
    """
    eyes = Eyes()
    eyes.configure.batch = BatchInfo("GitLab Python Demo")
    eyes.configure.batch.id = os.environ["BITBUCKET_COMMIT"]
    eyes.configure.batch.notify_on_completion = True
    eyes.configure.server_url = "https://test5vmseyes.applitools.com"
    eyes.configure.api_key = os.environ["APPLITOOLS_API_KEY"]
    yield eyes
    # If the test was aborted before eyes.close was called,
    # ends the test as aborted.
    eyes.abort()


def test_tutorial(eyes):
    # Start the session and set app name and test name.
    eyes.open("Demo App - Images Python", "Smoke Test - Images Python")

    image = Image.open(
        io.BytesIO(
            requests.get("https://i.ibb.co/bJgzfb3/applitools.png").content))

    # Visual checkpoint #1.
    eyes.check_image(image)

    # End the test.
    eyes.close(False)

    batch_id = eyes.configure.batch.id
    batch_close = BatchClose()
    batch_close.server_url = eyes.configure.server_url
    batch_close.api_key = eyes.configure.api_key
    bc = batch_close.set_batch_ids(batch_id)
    bc.close()
